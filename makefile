.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := --coverage -pedantic -std=c++14 -O3 -I/usr/local/include -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
else ifeq ($(shell uname -p), unknown)
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := --coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov
    VALGRIND      := valgrind
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := --coverage -pedantic -std=c++14 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Collatz.log:
	git log > Collatz.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Collatz code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Collatz.cpp
	git add Collatz.hpp
	-git add Collatz.log
	-git add html
	git add makefile
	git add README.md
	git add RunCollatz.cpp
	git add RunCollatz.ctd
	git add RunCollatz.in
	git add RunCollatz.out
	git add TestCollatz.cpp
	git commit -m "another commit"
	git push
	git status

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunCollatz
	rm -f TestCollatz

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Collatz.log
	rm -f  Doxyfile
	rm -rf collatz-tests
	rm -rf html
	rm -rf latex

# compile run harness
RunCollatz: Collatz.hpp Collatz.cpp RunCollatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) RunCollatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp RunCollatz.cpp -o RunCollatz

# compile test harness
TestCollatz: Collatz.hpp Collatz.cpp TestCollatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) TestCollatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp TestCollatz.cpp -o TestCollatz $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunCollatz                            \
    TestCollatz

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	$(CHECKTESTDATA) RunCollatz.ctd RunCollatz.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunCollatz.ctd RunCollatz.tmp

# execute run harness and diff with expected output
run: RunCollatz
	./RunCollatz < RunCollatz.in > RunCollatz.tmp
	-diff RunCollatz.tmp RunCollatz.out

# execute test harness
test: TestCollatz
	$(VALGRIND) ./TestCollatz
	$(GCOV) -b Collatz.cpp | grep -B 2 "cpp.gcov"

# test files in the Collatz test repo
TFILES := `ls collatz-tests/*.in`

# clone the Collatz test repo
collatz-tests:
	git clone https://gitlab.com/gpdowning/cs371g-collatz-tests.git collatz-tests

# execute run harness against a test in Collatz test repo and diff with expected output
collatz-tests/%: RunCollatz
	./RunCollatz < $@.in > RunCollatz.tmp
	diff RunCollatz.tmp $@.out

# execute run harness against all tests in Collatz test repo and diff with expected output
tests: collatz-tests RunCollatz
	for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Collatz.cpp
	$(ASTYLE) Collatz.hpp
	$(ASTYLE) RunCollatz.cpp
	$(ASTYLE) TestCollatz.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Collatz.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        \
    collatz-tests

# uncomment these four lines when you've created those files
# you must replace GitLabID with your GitLabID
#    collatz-tests/GitLabID-RunCollatz.in  \
#    collatz-tests/GitLabID-RunCollatz.out \
#    Collatz.log                           \
#    ctd-check                             \
#    html

# check the existence of check files
check: $(CFILES)

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Darwin)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
