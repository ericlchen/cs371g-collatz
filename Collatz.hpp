// ---------
// Collatz.h
// ---------

/**
 * @file Collatz.h
 * @brief Solves the collatz problem for ranges 
 */
#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * Read two ints and output a pair
 * @param an istream_iterator
 * @return a pair of ints
 */
pair<int, int> collatz_read (istream_iterator<int>&);

// --------------
// collatz_helper
// --------------

/**
 * Perform Collatz on the range defined and output the max Collatz in that range.
 * @param an int
 * @param an int
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_helper (int i, int j);

// ------------
// collatz_eval
// ------------

/**
 * Perform Collatz on the range defined and output the max Collatz in the range.
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * Print three ints from a given tuple
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * Takes in input and solves collatz for given values.
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
