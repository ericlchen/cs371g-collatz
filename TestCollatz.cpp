// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(600000, 800000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  600000);
    ASSERT_EQ(j, 800000);
    ASSERT_EQ(v, 509);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(150000, 350000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  150000);
    ASSERT_EQ(j, 350000);
    ASSERT_EQ(v, 443);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(1500, 3500));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1500);
    ASSERT_EQ(j, 3500);
    ASSERT_EQ(v, 217);
}

TEST(CollatzFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(350000, 150000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  350000);
    ASSERT_EQ(j, 150000);
    ASSERT_EQ(v, 443);
}

TEST(CollatzFixture, eval8) {
    tuple<int, int, int> t = collatz_eval(make_pair(3500, 1500));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  3500);
    ASSERT_EQ(j, 1500);
    ASSERT_EQ(v, 217);
}

TEST(CollatzFixture, eval9) {
    tuple<int, int, int> t = collatz_eval(make_pair(210, 201));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  210);
    ASSERT_EQ(j, 201);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval10) {
    tuple<int, int, int> t = collatz_eval(make_pair(1000, 900));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1000);
    ASSERT_EQ(j, 900);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval11) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 999999));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 999999);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval12) {
    tuple<int, int, int> t = collatz_eval(make_pair(999999, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  999999);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval13) {
    tuple<int, int, int> t = collatz_eval(make_pair(2500, 3500));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  2500);
    ASSERT_EQ(j, 3500);
    ASSERT_EQ(v, 217);
}

TEST(CollatzFixture, eval14) {
    tuple<int, int, int> t = collatz_eval(make_pair(3500, 2500));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  3500);
    ASSERT_EQ(j, 2500);
    ASSERT_EQ(v, 217);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}
