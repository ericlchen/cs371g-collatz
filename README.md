# CS371g: Generic Programming Collatz Repo

* Name: Eric Chen

* EID: elc2637

* GitLab ID: ericlchen

* HackerRank ID: ericlchen

* Git SHA: 4ef2a7491d33e77ff0dd76df7a0bb0eb5120fcc4

* GitLab Pipelines: https://gitlab.com/ericlchen/cs371g-collatz/-/pipelines

* Estimated completion time: 10 hours

* Actual completion time: 12 hours

* Comments: Doxygen seems to not work with just the hpp file. Set EXTRACT_ALL = YES and EXTRACT_PRIVATE = YES to get documentation.
